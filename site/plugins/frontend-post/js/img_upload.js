jQuery(document).ready(function() { 
	var options = { 
		beforeSend: function() {
			jQuery("#progress").show();
			jQuery("#bar").width('0%');
			jQuery("#message").html("");
			jQuery("#percent").html("0%");
			jQuery( ".aspk_paste_text" ).unbind();
		},
		uploadProgress: function(event, position, total, percentComplete) {
			jQuery("#bar").width(percentComplete+'%');
			jQuery("#percent").html(percentComplete+'%');
		},
		success: function() {
			jQuery("#bar").width('100%');
			jQuery("#percent").html('100%');
		},
		complete: function(response) {
			jQuery("#message").html("<font color='green'>"+response.responseText+"</font>");
			jQuery("#progress").hide();
		},
		error: function() {
			jQuery("#message").html("<font color='red'> ERROR: unable to upload files</font>");
			jQuery("#btn_browse").css("cursor","pointer");
			jQuery("#btn_browse").removeAttr("disabled");
			jQuery("#uploadBtn").css("cursor","pointer");
			jQuery("#uploadBtn").removeAttr("disabled");
		}
	}; 
	var options2 = { 
		beforeSend: function() {},
		uploadProgress: function(event, position, total, percentComplete) {},
		success: function() {},
		complete: function(response) { 
			jQuery("#1-image").html(response.responseText);
		},
		error: function() {}
	}; 
	jQuery("#frm_file_upload").ajaxForm(options);
	jQuery("#frm_user_img").ajaxForm(options2);
});