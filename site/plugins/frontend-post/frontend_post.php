<?php
/*
Plugin Name: Front-end Post 
Plugin URI: 
Description: It is used to create post from front-end without signing-in.
Author: Agile Solutions PK
Version: 1.9
Author URI: http://Agilesolutionspk.com
*/
//function alog($a,$b,$c,$d){}
	
if ( !class_exists( 'Agile_Front_End_Post' )){
	class Agile_Front_End_Post{
		
		function __construct() {
			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
			add_action('admin_enqueue_scripts', array(&$this, 'scripts_method') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action('admin_footer', array(&$this, 'admin_footer'));
			add_action( 'wp_ajax_aspk_delete_image', array(&$this,'agile_delete_attachment_ajax') );
			add_action( 'wp_ajax_nopriv_aspk_delete_image', array(&$this,'agile_delete_attachment_ajax') );
			add_action( 'wp_ajax_nopriv_aspk_ajax_upload', array(&$this,'aspk_upload_action_callback') );
			add_action( 'wp_ajax_aspk_ajax_upload', array(&$this,'aspk_upload_action_callback') );
			
			add_action( 'wp_ajax_aspk_ajax_upload_u', array(&$this,'aspk_upload_action_callback_u') );
			add_action( 'wp_ajax_nopriv_aspk_ajax_upload_u', array(&$this,'aspk_upload_action_callback_u') );

			
			add_shortcode( 'agile_aspk_fe_post_form', array(&$this, 'aspk_fe_post_form') );
			add_shortcode( 'agile_aspk_fe_posts_per_cate', array(&$this, 'aspk_fe_posts_per_cate') );
			add_shortcode( 'agile_aspk_fe_post_single_cate', array(&$this, 'aspk_fe_post_single_cate') );
			add_shortcode( 'agile_aspk_fe_post_list_cate', array(&$this, 'aspk_fe_post_list_cate') );
			register_activation_hook( __FILE__, array($this, 'install') );
			
			//register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_filter('do_parse_request',array(&$this,'request'),9,3);
			//add_filter( 'template_include',array(&$this, 'include_custom_template' ) );
			//add_filter( 'single_template',array(&$this, 'include_custom_template' ) );
			add_action( 'template_redirect', array(&$this, 'aspk_agile_redirect_post' ));
			add_action( 'wp_head', array(&$this, 'head' ));
		}
		/*function include_custom_template($template ){
			$template = plugin_dir_path( __FILE__ ) . 'single-fe_posts.php';
			return $template;
		}*/
		
		function head(){
			echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
		}
		
		function aspk_agile_redirect_post(){
			if(isset($_GET['fe_id'])){
			
				$path=$_SERVER['REQUEST_URI'];
				if (strpos($path,'fe_posts') !== false) {
					global $wpdb;
					$slug = basename($path);
					$sql="SELECT ID FROM {$wpdb->prefix}posts WHERE post_name = '{$slug}'";
					$id = $wpdb->get_var($sql);
					$page_id = get_option('_agile_aspk_fe_post_single', -1);
					$url = get_permalink( $page_id );
					$redirect_link = $url.'?fe_id='.$id;
					if(empty($id)) return;
					?>
					<script>
					window.location.href = '<?php echo $redirect_link;?>';
					</script>
					<?
				}
			}
		}
		function fe_init(){
			//ob_start();
			if (!session_id()) {
				session_start();
			}
			wp_enqueue_style( 'aspk_width_for_frontend', plugins_url('css/frontendwidth.css', __FILE__) );
			$this->setup_custom_post();
		}
		function aspk_fe_post_list_cate(){
			ob_start();
			$terms = get_terms( 'fecategory', 'orderby=count&hide_empty=0' );
			$page_id = get_option('_agile_aspk_fe_post_list', -1);
			$url = get_permalink( $page_id );
			foreach($terms as $term){
				$redirect_link = $url.'?cate_id='.$term->term_id;
				
				?>
				<div class="tw-bs container">
					<div class="row">
						<div class="col-xs-12">	
						<a href="<?php echo $redirect_link;?>">
								<div class="btn btn-primary">
									
										<?php echo $term->name;?>
									
								</div>
						</a>
							<div class="line1"></div>
						</div><!--ends col-->
					</div><!--end row-->
				</div><!--container ends-->
				
				<?php
			}
			$outputcnt = ob_get_clean ( );
			return $outputcnt;
		}
		function aspk_fe_posts_per_cate(){
			ob_start();
			$posts = get_posts(array(
					'post_type' => 'aspk_fe_post',
					'tax_query' => array(
						array(
							'taxonomy' => 'fecategory',
							'field' => 'term_id',
							'terms' => $_GET['cate_id'])
					),
					'orderby' => 'ID', 
					'order' => 'DESC'
				)
			);
			
			
			$page_id = get_option('_agile_aspk_fe_post_single', -1);
			$url = get_permalink( $page_id );
			foreach($posts as $post){
				$redirect_link = $url.'?fe_id='.$post->ID;
				?>
				<a href="<?php echo $redirect_link;?>" style="text-decoration:none;">
					<div class="pri_container">
						<div class="seg">
							<div class="rowa">
								<?php 
								echo $post->post_title;
								?>
							</div>
						</div>
					</div>
				</a>
				<?php
			}
			$outputcnt = ob_get_clean ( );
			return $outputcnt;
		}
		function aspk_fe_post_single_cate(){
			ob_start();
			if(isset($_GET['fe_id'])){
				if(! is_numeric( $_GET['fe_id'])){
					return "No Post Available";
				}
			}else{
				return "No Post Available";
			}
			$post= get_post($_GET['fe_id']);
			
			$terms = get_the_terms($post->ID, 'fecategory');

			foreach($terms as $t){
				$term  = $t->name;
			}
			$optional_data = get_post_meta( $post->ID, 'fe_optional_data', true );
			
			
			$args = array(
				'post_content' => 'set',
				'post_parent' => $post->ID,
				'post_status' => 'inherit',
				'post_type' => 'attachment',
			);
            $attachments = get_children( $args );
			//alog('$attachments2',$attachments,__FILE__,__LINE__);
			$arg = array(
				'post_content' => 'single',
				'post_parent' => $post->ID,
				'post_status' => 'inherit',
				'post_type' => 'attachment',
			);
            $att_user= get_children( $arg );
			//alog('$att_user3', $att_user,__FILE__,__LINE__);
			?>
			<div class="tw-bs container">
				<div class="seg">
					<div class="rowa"><?php echo $post->post_title; ?></div>
					<div class="rowa"><?php echo "Category :".$term; ?></div>
					<?php
					foreach($attachments as $at){
						
						if($at->post_content=='set'){
							?>
							<div class="rowa">
								<img src="<?echo $at->guid;?>"/>
							</div>
							<?php
						}
					}
					?>
					<div class="rowa"><div class="btn btn-primary"><?php echo "Description";?></div></div>	
					<div class="rowa"><?php echo $post->post_content;?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Name";?></div></div>
					<div class="rowa"><?php if($optional_data['fe_name']!=''){echo $optional_data['fe_name'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Address";?></div></div>	
					<div class="rowa"><?php if($optional_data['fe_address']!=''){echo $optional_data['fe_address'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "City";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_city_town']!=''){echo $optional_data['fe_city_town'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "State";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_state']!=''){echo $optional_data['fe_state'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Zip code";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_zip']!=''){echo $optional_data['fe_zip'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Website";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_website']!=''){echo $optional_data['fe_website'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "E-mail";?></div></div>	
					<div class="rowa"><?php if($optional_data['fe_email']!=''){echo $optional_data['fe_email'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Phone";?></div>	</div>
					<div class="rowa"><?php if($optional_data['e_phone']!=''){echo $optional_data['e_phone'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "Twitter";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_twitter']!=''){echo $optional_data['fe_twitter'];}else {echo"None";};?></div>	
					<div class="line1"></div>
					
					<div class="rowa"><div class="btn btn-primary"><?php echo "YouTube";?></div>	</div>
					<div class="rowa"><?php if($optional_data['fe_utube']!=''){echo $optional_data['fe_utube'];}else {echo"None";};?></div>	
					
					<?php
					foreach($att_user as $att){
						if($att->post_content=='single'){
							?>
							<div class="rowa">
								<img src="<?echo $att->guid;?>" />
							</div>
							<?php
						}
					}
					?>
				</div>
			</div>
			<?
			$outputcnt = ob_get_clean ( );
			return $outputcnt;
		}
		function aspk_fe_post_form(){
			ob_start();
			if(isset($_POST['fe_title'])){
				$fe_categ = $_POST['fe_category'];
				$fe_title = $_POST['fe_title'];
				$fe_descr = $_POST['fe_description'];
				if(($fe_categ=='') || ($fe_title=='') || ($fe_descr=='')){
					if($fe_categ==''){$x_categ ='style=" border: 2px solid #F78181;background:#F8E0E0;"';}
					if($fe_title==''){$x_title ='style=" border: 2px solid #F78181;background:#F8E0E0;"';}
					if($fe_descr==''){$x_descr ='style=" border: 2px solid #F78181;background:#F8E0E0;"';}

				}else{
					$this->insert_fe_post_with_all_attachments();
				}
			}
			wp_enqueue_script('tw_bs',plugins_url('js/bootstrap.min.js', __FILE__));
			?>
			<div class="tw-bs container">
				<form method="post" action="" id="aspk_details" role="form">
				<div class="row">
					<div class="col-xs-12">
							<div class="rowa">Post Article:(No Registration needed)</div>
					</div><!--ends col-->
				</div><!--ends row1-->
				<div class="row">
					<div class="col-xs-12">
						<div class="rowa">Category</div>
					</div><!--ends col-->
				</div><!--ends row2-->
				<div class="row">
					<div class="col-xs-12">
						<?php $terms = get_terms( 'fecategory', 'orderby=count&hide_empty=0' );?>
						<select class="form-control" id="aspk_req_cata"  name="fe_category" >
							<option value="">Select a Category</option>
							<?php 
							foreach($terms as $term){
								?>
								<option value="<?php echo $term->name;?>"><?php echo $term->name;?></option>
								<?php
							}?>
						</select>
					</div><!--ends col-->
				</div><!--ends row2-->
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Title</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa"><input class ="form-control" id="aspk_req_title" type="text"  name="fe_title" required <?php if(isset($x_title))echo $x_title;?>/></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Description</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<textarea style="height:10em;" class ="form-control" id="aspk_req_description" name="fe_description" rows="4"> <?php if(isset($x_descr))echo $x_descr;?></textarea>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Name : ( Optional ) Show my name under post</div>
					</div><!--ends col-->
				</div>	
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" id="aspk_name" type="text"  name="fe_name" placeholder="Enter name to show under post"/>
					</div><!--ends col-->
				</div>			
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Address : ( Optional ) Show my address under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_address" placeholder="Enter address to show under post"/>
					</div><!--ends col-->
				</div>			
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">City/Town : ( Optional ) Show my city/town under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_city_town" placeholder="Enter city/town to show under post"/>
					</div><!--ends col-->
				</div>			
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">State : ( Optional ) Show my state under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_state" placeholder="Enter name to state under post"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Zip code : ( Optional ) Show my zip code under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_zip" placeholder="Enter zip code to show under post"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Website : ( Optional ) Show my website URL under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_website" placeholder="Example :  http://wordpress.com"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">E-mail : ( Optional ) Show my e-mail address under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_email" placeholder="Enter e-mail address to show under post"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Phone : ( Optional ) Show my phone number under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_phone" placeholder="Enter phone number to show under post"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Twitter : ( Optional ) Show my Twitter Link under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_twitter" placeholder="Enter your Twitter user-name"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">YouTube : ( Optional ) Show my YouTube Link under post</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control" type="text" name="fe_utube" placeholder="Enter your YouTube user-name"/>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Add up to 8 images - (jpg, png, gif)</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<!-- start fine uploader patch -->
						<script type="text/javascript">
							var fileDefaultText = 'No file selected';
							var fileBtnText     = 'Choose File';
						</script>
						<div class="rowa">
							<ul id="error_list"></ul>
							<form name="item8" action="" method="post" enctype="multipart/form-data">
								<input type="hidden" name="CSRFName" value="CSRF1230800161_1933958787">
								<input type="hidden" name="CSRFToken" value="d2ba38c1e75f4f569eb9dd119c14543ed89f9c1581f6052f5b82783792cf2d6064d926a919976745fe97fa5d6b946adc434e5175d453bfeaa0b5c38f70b761d5">
								<input type="hidden" name="action" value="item_add_post">
								<input type="hidden" name="page" value="item">
								<div class="box photos">
									<div id="restricted-fine-uploader">
										<div class="qq-uploader">
											<div class="qq-upload-drop-area" style="display: none;">
												<span>Drop files here to upload</span>
											</div>
											<div class="qq-upload-button" style="position: relative; overflow: hidden; direction: ltr;">
												<div>Click or Drop for upload images</div>
												<input multiple="multiple" type="file" name="qqfile" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;">
											</div>
											<span class="qq-drop-processing">
												<span>Processing dropped files...</span>
												<span class="qq-drop-processing-spinner"></span>
											</span>
											<ul class="qq-upload-list"></ul>
										</div>
									</div>
								</div>
							</form>

							<script>
								jQuery(document).ready(function($) {
									$('.qq-upload-delete').on('click', function(evt) {
										evt.preventDefault();
										var parent = $(this).parent()
										var result = confirm('This action can\'t be undone. Are you sure you want to continue?');
										var urlrequest = '';
										if($(this).attr('ajaxfile')!=undefined) {
											urlrequest = 'ajax_photo='+$(this).attr('ajaxfile');
										} else {
											urlrequest = 'id='+$(this).attr('photoid')+'&item='+$(this).attr('itemid')+'&code='+$(this).attr('photoname')+'&secret='+$(this).attr('photosecret');
										}
										var image_att_id = $(this).attr('ajaxfile');
										if(result) {
											$.ajax({
												type: "POST",
												//url: 'http://www.classifiedsclone.com/abc/index.php?page=ajax&action=delete_image&'+urlrequest,
												url: '<?php echo admin_url('admin-ajax.php?action=aspk_delete_image&id');?>'+image_att_id,
												dataType: 'json',
												success: function(data){
													parent.remove();
												}
											});
										}
									});

									$('#restricted-fine-uploader').on('click','.primary_image', function(event){
										if(parseInt($("div.primary_image").index(this))>0){

											var a_src   = $(this).parent().find('.ajax_preview_img img').attr('src');
											var a_title = $(this).parent().find('.ajax_preview_img img').attr('alt');
											var a_input = $(this).parent().find('input').attr('value');
											// info
											var a1 = $(this).parent().find('span.qq-upload-file').text();
											var a2 = $(this).parent().find('span.qq-upload-size').text();

											var li_first =  $('ul.qq-upload-list li').get(0);

											var b_src   = $(li_first).find('.ajax_preview_img img').attr('src');
											var b_title = $(li_first).find('.ajax_preview_img img').attr('alt');
											var b_input = $(li_first).find('input').attr('value');
											var b1      = $(li_first).find('span.qq-upload-file').text();
											var b2      = $(li_first).find('span.qq-upload-size').text();

											$(li_first).find('.ajax_preview_img img').attr('src', a_src);
											$(li_first).find('.ajax_preview_img img').attr('alt', a_title);
											$(li_first).find('input').attr('value', a_input);
											$(li_first).find('span.qq-upload-file').text(a1);
											$(li_first).find('span.qq-upload-size').text(a2);

											$(this).parent().find('.ajax_preview_img img').attr('src', b_src);
											$(this).parent().find('.ajax_preview_img img').attr('alt', b_title);
											$(this).parent().find('input').attr('value', b_input);
											$(this).parent().find('span.qq-upload-file').text(b1);
											$(this).parent().find('span.qq-upload-file').text(b2);
										}
									});

									$('#restricted-fine-uploader').on('click','.primary_image', function(event){
										$(this).addClass('over primary');
									});

									$('#restricted-fine-uploader').on('mouseenter mouseleave','.primary_image', function(event){
										if(event.type=='mouseenter') {
											if(!$(this).hasClass('primary')) {
												$(this).addClass('primary');
											}
										} else {
											if(parseInt($("div.primary_image").index(this))>0){
												$(this).removeClass('primary');
											}
										}
									});

									var ajaxurl = '<?php echo admin_url('admin-ajax.php?action=aspk_ajax_upload'); ?>';
									$('#restricted-fine-uploader').on('mouseenter mouseleave','li.qq-upload-success', function(event){
										if(parseInt($("li.qq-upload-success").index(this))>0){
											if(event.type=='mouseenter') {
												$(this).find('div.primary_image').addClass('over');
											} else {
												$(this).find('div.primary_image').removeClass('over');
											}
										}
									});

									window.removed_images = 0;
									$('#restricted-fine-uploader').on('click', 'a.qq-upload-delete', function(event){
										window.removed_images = window.removed_images+1;
										$('#restricted-fine-uploader .flashmessage-error').remove();
									});

									$('#restricted-fine-uploader').fineUploader({
										request: {
											endpoint: ajaxurl
										},
										multiple: true,
										validation: {
											allowedExtensions: ['png','gif','jpg'],
											sizeLimit: 2097152,
											itemLimit: 8
										},
										messages: {
											tooManyItemsError: 'Too many items ({netItems}) would be uploaded. Item limit is {itemLimit}.',
											onLeave: 'The files are being uploaded, if you leave now the upload will be cancelled.',
											typeError: '{file} has an invalid extension. Valid extension(s): {extensions}.',
											sizeError: '{file} is too large, maximum file size is {sizeLimit}.',
											emptyError: '{file} is empty, please select files again without it.'
										},
										deleteFile: {
											enabled: true,
											method: "POST",
											forceConfirm: false,
											//endpoint: 'http://www.classifiedsclone.com/abc/index.php?page=ajax&action=delete_ajax_upload'
											endpoint: '<?php echo admin_url('admin-ajax.php?action=aspk_delete_image');?>'
										},
										retry: {
											showAutoRetryNote : true,
											showButton: true
										},
										text: {
											uploadButton: 'Click or Drop for upload images',
											waitingForResponse: 'Processing...',
											retryButton: 'Retry',
											cancelButton: 'Cancel',
											failUpload: 'Upload failed',
											deleteButton: 'Delete',
											deletingStatusText: 'Deleting...',
											formatProgress: '{percent}% of {total_size}'
										}
									}).on('error', function (event, id, name, errorReason, xhrOrXdr) {
										$('#restricted-fine-uploader .flashmessage-error').remove();
										$('#restricted-fine-uploader').append('<div class="flashmessage flashmessage-error">' + errorReason + '</div>');
									}).on('statusChange', function(event, id, old_status, new_status) {
										$(".alert.alert-error").remove();
									}).on('complete', function(event, id, fileName, responseJSON) {
										if (responseJSON.success) {
											var new_id = id - removed_images;
											var li = $('.qq-upload-list li')[new_id];
											var url_img = responseJSON.img1;
											if(parseInt(new_id)==0) {
												$(li).append('<div class="primary_image primary"></div>');
											} else {
												$(li).append('<div class="primary_image"><a title="Make primary image"></a></div>');
											}
											$(li).append('<div class="ajax_preview_img"><img src="'+url_img+'" alt="' + responseJSON.uploadName+'"></div>');
											$(li).append('<input type="hidden" name="ajax_photos[]" value="'+responseJSON.uploadName+'"></input>');
										}
									});
								});
							</script>
						</div>
						<!-- end fine uploader patch -->
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">Upload Your Photo or Logo - (jpg, png, gif)</div>
						<script type="text/javascript">
							var fileDefaultText = 'No file selected';
							var fileBtnText     = 'Choose File';
						</script>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="rowa">
							<ul id="error_list"></ul>
							<form name="item" action="" method="post" enctype="multipart/form-data">
								<input type="hidden" name="CSRFName" value="CSRF1230800161_1933958787">
								<input type="hidden" name="CSRFToken" value="d2ba38c1e75f4f569eb9dd119c14543ed89f9c1581f6052f5b82783792cf2d6064d926a919976745fe97fa5d6b946adc434e5175d453bfeaa0b5c38f70b761d5">
								<input type="hidden" name="action" value="item_add_post">
								<input type="hidden" name="page" value="item">
								<div class="box photos">
									<div id="restricted-fine-uploader-u">
										<div class="qq-uploader">
											<div class="qq-upload-drop-area" style="display: none;">
												<span>Drop files here to upload</span>
											</div>
											<div class="qq-upload-button" style="position: relative; overflow: hidden; direction: ltr;">
												<div>Click or Drop for upload images</div>
												<input multiple="multiple" type="file" name="qqfile" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;">
											</div>
											<span class="qq-drop-processing">
												<span>Processing dropped files...</span>
												<span class="qq-drop-processing-spinner"></span>
											</span>
											<ul class="qq-upload-list"></ul>
										</div>
									</div>
								</div>
							</form>
							<script>
								jQuery(document).ready(function($) {
									$('.qq-upload-delete').on('click', function(evt) {
										evt.preventDefault();
										var parent = $(this).parent()
										var result = confirm('This action can\'t be undone. Are you sure you want to continue?');
										var urlrequest = '';
										if($(this).attr('ajaxfile')!=undefined) {
											urlrequest = 'ajax_photo='+$(this).attr('ajaxfile');
										} else {
											urlrequest = 'id='+$(this).attr('photoid')+'&item='+$(this).attr('itemid')+'&code='+$(this).attr('photoname')+'&secret='+$(this).attr('photosecret');
										}
										var image_att_id = $(this).attr('ajaxfile');
										if(result) {
											$.ajax({
												type: "POST",
												//url: 'http://www.classifiedsclone.com/abc/index.php?page=ajax&action=delete_image&'+urlrequest,
												url: '<?php echo admin_url('admin-ajax.php?action=aspk_delete_image&id');?>'+image_att_id,
												dataType: 'json',
												success: function(data){
													parent.remove();
												}
											});
										}
									});

									$('#restricted-fine-uploader-u').on('click','.primary_image', function(event){
										if(parseInt($("div.primary_image").index(this))>0){

											var a_src   = $(this).parent().find('.ajax_preview_img img').attr('src');
											var a_title = $(this).parent().find('.ajax_preview_img img').attr('alt');
											var a_input = $(this).parent().find('input').attr('value');
											// info
											var a1 = $(this).parent().find('span.qq-upload-file').text();
											var a2 = $(this).parent().find('span.qq-upload-size').text();

											var li_first =  $('ul.qq-upload-list li').get(0);

											var b_src   = $(li_first).find('.ajax_preview_img img').attr('src');
											var b_title = $(li_first).find('.ajax_preview_img img').attr('alt');
											var b_input = $(li_first).find('input').attr('value');
											var b1      = $(li_first).find('span.qq-upload-file').text();
											var b2      = $(li_first).find('span.qq-upload-size').text();

											$(li_first).find('.ajax_preview_img img').attr('src', a_src);
											$(li_first).find('.ajax_preview_img img').attr('alt', a_title);
											$(li_first).find('input').attr('value', a_input);
											$(li_first).find('span.qq-upload-file').text(a1);
											$(li_first).find('span.qq-upload-size').text(a2);

											$(this).parent().find('.ajax_preview_img img').attr('src', b_src);
											$(this).parent().find('.ajax_preview_img img').attr('alt', b_title);
											$(this).parent().find('input').attr('value', b_input);
											$(this).parent().find('span.qq-upload-file').text(b1);
											$(this).parent().find('span.qq-upload-file').text(b2);
										}
									});

									$('#restricted-fine-uploader-u').on('click','.primary_image', function(event){
										$(this).addClass('over primary');
									});

									$('#restricted-fine-uploader-u').on('mouseenter mouseleave','.primary_image', function(event){
										if(event.type=='mouseenter') {
											if(!$(this).hasClass('primary')) {
												$(this).addClass('primary');
											}
										} else {
											if(parseInt($("div.primary_image").index(this))>0){
												$(this).removeClass('primary');
											}
										}
									});

									var ajaxurl = '<?php echo admin_url('admin-ajax.php?action=aspk_ajax_upload_u'); ?>';
									$('#restricted-fine-uploader-u').on('mouseenter mouseleave','li.qq-upload-success', function(event){
										if(parseInt($("li.qq-upload-success").index(this))>0){
											if(event.type=='mouseenter') {
												$(this).find('div.primary_image').addClass('over');
											} else {
												$(this).find('div.primary_image').removeClass('over');
											}
										}
									});

									window.removed_images = 0;
									$('#restricted-fine-uploader-u').on('click', 'a.qq-upload-delete', function(event){
										window.removed_images = window.removed_images+1;
										$('#restricted-fine-uploader-u .flashmessage-error').remove();
									});

									$('#restricted-fine-uploader-u').fineUploader({
										request: {
											endpoint: ajaxurl
										},
										multiple: true,
										validation: {
											allowedExtensions: ['png','gif','jpg'],
											sizeLimit: 2097152,
											itemLimit: 1
										},
										messages: {
											tooManyItemsError: 'Too many items ({netItems}) would be uploaded. Item limit is {itemLimit}.',
											onLeave: 'The files are being uploaded, if you leave now the upload will be cancelled.',
											typeError: '{file} has an invalid extension. Valid extension(s): {extensions}.',
											sizeError: '{file} is too large, maximum file size is {sizeLimit}.',
											emptyError: '{file} is empty, please select files again without it.'
										},
										deleteFile: {
											enabled: true,
											method: "POST",
											forceConfirm: false,
											//endpoint: 'http://www.classifiedsclone.com/abc/index.php?page=ajax&action=delete_ajax_upload'
											endpoint: '<?php echo admin_url('admin-ajax.php?action=aspk_delete_image');?>'
										},
										retry: {
											showAutoRetryNote : true,
											showButton: true
										},
										text: {
											uploadButton: 'Click or Drop for upload images',
											waitingForResponse: 'Processing...',
											retryButton: 'Retry',
											cancelButton: 'Cancel',
											failUpload: 'Upload failed',
											deleteButton: 'Delete',
											deletingStatusText: 'Deleting...',
											formatProgress: '{percent}% of {total_size}'
										}
									}).on('error', function (event, id, name, errorReason, xhrOrXdr) {
										$('#restricted-fine-uploader-u .flashmessage-error').remove();
										$('#restricted-fine-uploader-u').append('<div class="flashmessage flashmessage-error">' + errorReason + '</div>');
									}).on('statusChange', function(event, id, old_status, new_status) {
										$(".alert.alert-error").remove();
									}).on('complete', function(event, id, fileName, responseJSON) {
										if (responseJSON.success) {
											var new_id = id - removed_images;
											//var li = $('.qq-upload-list li')[new_id];
											var li = $('#restricted-fine-uploader-u div ul li')[new_id];
											var url_img = responseJSON.img1;
											if(parseInt(new_id)==0) {
												$(li).append('<div class="primary_image primary"></div>');
											} else {
												$(li).append('<div class="primary_image"><a title="Make primary image"></a></div>');
											}
											$(li).append('<div class="ajax_preview_img"><img src="'+url_img+'" alt="' + responseJSON.uploadName+'"></div>');
											$(li).append('<input type="hidden" name="ajax_photos[]" value="'+responseJSON.uploadName+'"></input>');
										}
									});
								});
							</script>
						</div>
					</div><!--ends col-->
				</div>
				<div class="row">
					<div class="col-xs-12">			
						<div class="line1"></div>
					</div><!--ends col-->
				</div>

				<div class="row">
					<div class="col-xs-12">			
						<input class ="form-control btn btn-primary" type="button"  id="agile_spk_publish" value="Publish" onClick='submitDetailsForm()'/>
						<script>
								function submitDetailsForm() {
								   jQuery("#aspk_details").submit();
								}
						</script>
					</div><!--ends col-->
				</div>
				
				
			</div><!--ends container-->
			<!--</form>-->
			<?php
			$outputcnt = ob_get_clean ( );
			return $outputcnt;
		}
		function aspk_upload_action_callback(){
			//alog('upload callback',$_FILES,__FILE__,__LINE__);
			//alog('requrest',$_REQUEST,__FILE__,__LINE__);
			$this->ajax_upfile();
		}
		function aspk_upload_action_callback_u(){
			//alog('upload callback',$_FILES,__FILE__,__LINE__);
			//alog('requrest',$_REQUEST,__FILE__,__LINE__);
			$this->ajax_user_img();
		}
		
		function request($goahead, $t, $v){
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$targeturl = home_url().'/index.php?aspk_up_file=80420';
			if($url == $targeturl){
				if(isset($_POST['aspk_file_345'])){
					$this->ajax_upfile();
					
					return false;
				}
			}
			$targeturl = home_url().'/index.php?aspk_user_img=80420';
			if($url == $targeturl){
				if(isset($_POST['aspk_file_346'])){
					$this->ajax_user_img();		
					return false;
				}
			}
			return $goahead;
		}
		function insert_fe_post_with_all_attachments(){
			
			$fe_post = array(
				"fe_name" => $_POST['fe_name'],
				"fe_address" => $_POST['fe_address'],
				"fe_city_town" => $_POST['fe_city_town'],
				"fe_state" => $_POST['fe_state'],
				"fe_zip" => $_POST['fe_zip'],
				"fe_website" => $_POST['fe_website'],
				"fe_email" => $_POST['fe_email'],
				"e_phone" => $_POST['e_phone'],
				"fe_twitter" => $_POST['fe_twitter'],
				"fe_utube" => $_POST['fe_utube'],
			);
			$my_post = array(
				'post_title'    => $_POST['fe_title'],
				'post_content'  => $_POST['fe_description'],
				'post_status'   => 'publish',
				'post_type'     => 'aspk_fe_post',
				'post_author'   => 0,
				'post_category' => array( $_POST['fe_category'] )
			);
			
			$post_ID = wp_insert_post( $my_post );
			wp_set_object_terms( $post_ID, array( $_POST['fe_category'] ), 'fecategory' );
			update_post_meta($post_ID, 'fe_optional_data', $fe_post);
			
			global $wpdb;
			$para = array();
			$para['prefix'] = $wpdb->prefix;
			$para['sessionid'] = session_id();
			//alog('$rs',$rs,__FILE__,__LINE__);
			$rs = $this->get_sql('select_session_images', $para);
			//alog('$rs',$rs,__FILE__,__LINE__);
			$del='';
			foreach($rs as $r){
				$title= basename($r->path, ".png");
				$title= basename($title, ".jpg");
				$title= basename($title, ".gif");
				$title= basename($title, ".jpeg");
				$filetype = wp_check_filetype( basename( $r->path ), null );

				$attachment = array(
					'guid'           => $r->url, 
					'post_mime_type' => $filetype['type'],
					'post_title'     => $title,
					'post_content'   => 'set',
					'post_status'    => 'inherit'
				);

				wp_insert_attachment( $attachment, $r->path, $post_ID );
				$del .= $r->id.',';
			}
			$del = rtrim($del, ",");
			$sql="DELETE FROM {$wpdb->prefix}agile_images_sessions where id in (".$del.")";
			$wpdb->query($sql);	
			
			$rs = $this->get_sql('select_session_user_images', $para);
			foreach($rs as $r){
				$title= basename($r->path, ".png");
				$title= basename($title, ".jpg");
				$title= basename($title, ".gif");
				$title= basename($title, ".jpeg");
				$filetype = wp_check_filetype( basename( $r->path ), null );

				$attachment = array(
					'guid'           => $r->url, 
					'post_mime_type' => $filetype['type'],
					'post_title'     => $title,
					'post_content'   => 'single',
					'post_status'    => 'inherit'
				);
				
				wp_insert_attachment( $attachment, $r->path, $post_ID );
				$sql="DELETE FROM {$wpdb->prefix}agile_images_sessions where id='{$r->id}'";
				$wpdb->query($sql);
			}
		}
		function agile_delete_attachment_ajax(){
			global $wpdb;
			if(isset($_REQUEST['qquuid'])){
				$qquuid = $_REQUEST['qquuid'];
				$sql="DELETE FROM {$wpdb->prefix}agile_images_sessions where qquuid='{$qquuid}'";
				$var = $wpdb->query($sql);	
				$img_return = array('success' => 'true');
					echo json_encode($img_return);
				exit;
			}
		}
		function ajax_user_img(){
			ob_end_clean();
			
			if (!function_exists('wp_handle_upload')) require_once(ABSPATH.'wp-admin/includes/file.php');
			$uploadedfile = $_FILES['qqfile'];
			
			$fileTypes = array('jpg','jpeg','png','gif','JPG','JPEG','PNG','GIF');
			$fileParts = pathinfo($_FILES['qqfile']['name']);
			
			if(in_array(strtolower($fileParts['extension']),$fileTypes)){
				$upload_overrides = array( 'test_form' => false );//otherwise upload will be rejected
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				if (!empty($movefile)) {
					$movefile['qquuid']= $_REQUEST['qquuid'];
				}
				
				$img_att_id = $this->store_update_select_user_images($movefile);
				$img_return = array( 
						'success' => 'true', 
						'uploadName' => 'image.jpg', 
						'img1' =>  $movefile['url']
				);
				//alog('$img_return',$img_return,__FILE__,__LINE__);
				echo json_encode($img_return);
				exit;
			}
			exit;
			
		}
		function ajax_upfile(){
			ob_end_clean();
					
			if (!function_exists('wp_handle_upload')) require_once(ABSPATH.'wp-admin/includes/file.php');
			$uploadedfile = $_FILES['qqfile'];
			
			if($uploadedfile != ''){
				$fileTypes = array('jpg','jpeg','png','gif','JPG','JPEG','PNG','GIF');
				$fileParts = pathinfo($_FILES['qqfile']['name']);
					
				if(in_array(strtolower($fileParts['extension']),$fileTypes)){
					$upload_overrides = array( 'test_form' => false );
					//otherwise upload will be rejected
					$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
					if (!empty($movefile)) {	
						$movefile['qquuid']= $_REQUEST['qquuid'];					
					}else{
						echo "Possible file upload attack!\n";
					}
					
					$img_att_id = $this->store_update_select_images($movefile);
					$img_return = array( 
						'success' => 'true', 
						'uploadName' => 'image.jpg', 
						'img1' =>  $movefile['url'], 
						'img_att_id'=> $img_att_id 
					);
					//alog('$img_return',$img_return,__FILE__,__LINE__);
					echo json_encode($img_return);
					exit;
				}else{
					echo '<font color="#8A0808">Invalid File Type</font>';
				}
			}else{
				echo '<font color="#8A0808">Please Browse File and then click on Upload</font>';
			}
			exit;
		}
		
		function admin_init(){
			/* Register our stylesheet. */
			
			if (!session_id()) {
				session_start();
			}
			$this->setup_custom_post();
		}
		function setup_custom_post(){
			
			
			register_post_type( 'aspk_fe_post',array('labels' => array('name' => __( 'FE Posts' ),'singular_name' => __( 'FE Post' )),'public' => true,'has_archive' => true,'rewrite' => array('slug' => 'fe_posts'),));
			
			$labels = array(
				'name'                       => _x( 'Category', 'taxonomy general name' ),
				'singular_name'              => _x( 'Category', 'taxonomy singular name' ),
				'search_items'               => __( 'Search Categories' ),
				'popular_items'              => __( 'Popular Categories' ),
				'all_items'                  => __( 'All Categories' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Category' ),
				'update_item'                => __( 'Update Category' ),
				'add_new_item'               => __( 'Add New Category' ),
				'new_item_name'              => __( 'New Category Name' ),
				'separate_items_with_commas' => __( 'Separate categories with commas' ),
				'add_or_remove_items'        => __( 'Add or remove categories' ),
				'choose_from_most_used'      => __( 'Choose from the most used categories' ),
				'not_found'                  => __( 'No categories found.' ),
				'menu_name'                  => __( 'Categories' ),
			);

			$args = array(
				'hierarchical'          => false,
				'labels'                => $labels,
				'show_ui'               => true,
				'show_admin_column'     => true,
				'update_count_callback' => '_update_post_term_count',
				'query_var'             => true,
				'rewrite'               => array( 'slug' => 'fecategory' ),
			);

			register_taxonomy( 'fecategory', 'aspk_fe_post', $args );
			
			register_taxonomy_for_object_type( 'fecategory', 'aspk_fe_post' );
		}
		function addAdminPage() {
			//add_menu_page('Petitions', 'Petitions', 'edit_pages', 'agile_petition', array(&$this, 'view_petition') );
			//add_submenu_page('agile_petition', 'Petition CSV', 'Petition CSV', 'read', 'agile_petition_creator_csv', array(&$this, 'creator_petition_csv'));
			add_submenu_page( 'edit.php?post_type=aspk_fe_post', 'Settings', 'Settings', 'manage_options', 'agile_aspk_fe_post_settings', array(&$this,'agile_aspk_fe_post_settings_callback' )); 
		}
		function remove_shortcode($id,$s){
			$p_base_page = get_post($id);
			$p_getpage = $p_base_page->post_content;
			$p_setpage = str_replace($s, "", $p_getpage);
			$p_base_page->post_content = $p_setpage;
			wp_update_post( $p_base_page );
		}
		function add_shortcode($id,$s){
			$base_page = get_post($id);
			$getpage = $base_page->post_content;
			$setpage = str_replace($s, "", $getpage);
			$setpage .= $s;
			$my_post = array('ID'=>$id,'post_content'=>$setpage);
			wp_update_post( $my_post );		
		}
		function agile_aspk_fe_post_settings_callback(){
			
			if(isset($_POST)){
				if(isset($_POST['setting_page'])){
					//delete short-code from previous page (form)
					$previous_fpage = get_option('_agile_aspk_fe_post_form', -1);
					$this->remove_shortcode($previous_fpage,'[agile_aspk_fe_post_form]');
					//add short-code to new page (form)
					$new_base_form = $_POST['fe_post_form'];
					$this->add_shortcode($new_base_form,'[agile_aspk_fe_post_form]');
					update_option( '_agile_aspk_fe_post_form', $new_base_form );
					//delete short-code from previous page (form)
					$previous_vpage = get_option('_agile_aspk_fe_post_list', -1);
					$this->remove_shortcode($previous_vpage,'[agile_aspk_fe_posts_per_cate]');
					//add short-code to new page (form)
					$new_base_view = $_POST['fe_post_list'];
					$this->add_shortcode($new_base_view,'[agile_aspk_fe_posts_per_cate]');
					update_option( '_agile_aspk_fe_post_list', $new_base_view );
					//delete short-code from previous page (form)
					$previous_fpage = get_option('_agile_aspk_fe_post_single', -1);
					$this->remove_shortcode($previous_fpage,'[agile_aspk_fe_post_single_cate]');
					//add short-code to new page (form)
					$new_base_form = $_POST['fe_post_single'];
					$this->add_shortcode($new_base_form,'[agile_aspk_fe_post_single_cate]');
					update_option( '_agile_aspk_fe_post_single', $new_base_form );
					//delete short-code from previous page (form)
					$previous_vpage = get_option('_agile_aspk_fe_post_category', -1);
					$this->remove_shortcode($previous_vpage,'[agile_aspk_fe_post_list_cate]');
					//add short-code to new page (form)
					$new_base_view = $_POST['fe_post_category'];
					$this->add_shortcode($new_base_view,'[agile_aspk_fe_post_list_cate]');
					update_option( '_agile_aspk_fe_post_category', $new_base_view );
					
				}
			}	
			?>
			<div class="pri_container">
				<h1>Settings</h1><hr/>
				<form action="" method="post">
					<div class="sec_container"></br>
						<h3>FE Post Base Page Setup</h3>
						<div class="rowa">
							<div class="col_left">Form</div>
							<div class="col_right">								
								<?php 
								$selected_form = get_option('_agile_aspk_fe_post_form', -1);
								echo $this->get_pages_html($selected_form,'fe_post_form');
								?>									
							</div>
						</div>
						<div class="rowa">
							<div class="col_left">FE Posts/Category</div>
							<div class="col_right">								
								<?php 
								$selected_form = get_option('_agile_aspk_fe_post_list', -1);
								echo $this->get_pages_html($selected_form,'fe_post_list');
								?>									
							</div>
						</div>
						<div class="rowa">
							<div class="col_left">Single FE Post</div>
							<div class="col_right">								
								<?php 
								$selected_form = get_option('_agile_aspk_fe_post_single', -1);
								echo $this->get_pages_html($selected_form,'fe_post_single');
								?>									
							</div>
						</div>
						<div class="rowa">
							<div class="col_left">Category List</div>
							<div class="col_right">								
								<?php 
								$selected_form = get_option('_agile_aspk_fe_post_category', -1);
								echo $this->get_pages_html($selected_form,'fe_post_category');
								?>									
							</div>
						</div>
					</div></br></br></br></br>
					<hr/>
					<input type="submit" name="setting_page" value="Save changes" class="button-primary">				
				</form>
			</div>
			<?php
		}
		function get_pages_html($selected_page_id, $s){
			//echo $selected_page_id;
			$args = array('posts_per_page' => 1000, 'post_type'=> 'page', 'post_status'=> 'publish');
			$all_pages = get_posts( $args );
			?>
			<select name="<?php echo $s;?>" id="<?php echo $s;?>" >
				<option value="all">---  select page ---</option>
				<?php	
				foreach($all_pages as $r){
					$selected = "";
					if(isset($selected_page_id)){
						if($selected_page_id == $r->ID) $selected = " selected ";
					}
					?>
					<option value="<?php echo $r->ID;?>" <?php echo $selected;?>><?php echo $r->post_title;?> </option>
					<?php
				}
				?>
			</select>
			<?php
		}
		function scripts_method(){
			wp_enqueue_script('jquery');
			//wp_enqueue_script('petition_admin', plugins_url('js/admin.js', __FILE__) );
			wp_enqueue_style( 'aspk_front_post_style_admin', plugins_url('css/admin.css', __FILE__) );
		}
		function frontend_scripts(){
			?>
				<script type="text/javascript">
				var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
				</script>
			<?php
			wp_enqueue_script('jquery');
			wp_enqueue_script('agile-images-uploader-front3',plugins_url('js/jquery.fineuploader.min.js', __FILE__));
			wp_enqueue_script('agile-images-uploader-front6',plugins_url('js/jquery.validate.min.js', __FILE__));
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('agile-images-uploader-front5',plugins_url('js/jquery.uniform.js', __FILE__));
			wp_enqueue_script('agile-images-uploader-front2',plugins_url('js/global.js', __FILE__));
			
			
			
			//wp_enqueue_style( 'aspk_front_post_style_boot', "//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" );
			wp_enqueue_style( 'aspk_front_post_style', plugins_url('css/front.css', __FILE__) );
			
			wp_enqueue_style( 'aspk_front_post_style_fine1', plugins_url('css/fineuploader.css', __FILE__) );
			wp_enqueue_style( 'aspk_front_post_style_fine4', plugins_url('css/style.css', __FILE__) );
			wp_enqueue_style( 'tw_bs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
		}
		function admin_footer() {
			
		}
		function store_update_select_images($movefile){
			global $wpdb;
			$para = array();
			$para['prefix'] = $wpdb->prefix;
			$para['sessionid'] = session_id();
			$rs = $this->get_sql('chk_sessionid_already_exist', $para);
			
			if($rs <= 7){
				$para['movefile'] = $movefile;
				$att_id = $this->get_sql('insert_image_aginst_sessionid', $para);
				return $att_id;
			}
		}
		function store_update_select_user_images($movefile){
			
			
			global $wpdb;
			$para = array();
			$para['prefix'] = $wpdb->prefix;
			$para['sessionid'] = session_id();
			$rs = $this->get_sql('chk_sessionid_already_exist_user_img', $para);
			
			$para['movefile'] = $movefile;
			if(empty($rs)){
				$this->get_sql('insert_user_image_aginst_sessionid', $para);
			}else{
				$para['id'] = $rs;
				$this->get_sql('update_user_image_aginst_sessionid', $para);
			}
			$rs = $this->get_sql('select_session_user_images', $para);
			return $rs;
		}
		function get_sql($sqlname, $para){
			global $wpdb;
			$pre = $para['prefix'];
			if($sqlname=='chk_sessionid_already_exist'){
				$s_id = $para['sessionid'];
				$sql="Select count(url) FROM {$wpdb->prefix}agile_images_sessions where sessionid='{$s_id}' and img='set'";
				$rs = $wpdb->get_var($sql);	
				return $rs;
			}
			if($sqlname=='insert_image_aginst_sessionid'){
				$dt = date("Y-m-d H:i:s");
				$movefile = $para['movefile'];
				$url = $movefile['url'];
				$path = $movefile['file'];
				$s_id = $para['sessionid'];
				$qquuid = $movefile['qquuid'];
				//alog('$para',$para,__FILE__,__LINE__);
				$img = 'set';
				$sql = "insert into {$pre}agile_images_sessions (sessionid,dt,url,path,img,qquuid) values('{$s_id}','{$dt}','{$url}','{$path}','{$img}','{$qquuid}')";
				$wpdb->query($sql);
				$att_id = $wpdb->insert_id;
				return $att_id;
			}
			if($sqlname=='update_user_image_aginst_sessionid'){
				$dt = date("Y-m-d H:i:s");
				$movefile = $para['movefile'];
				$url = $movefile['url'];
				$path = $movefile['file'];
				$s_id = $para['sessionid'];
				$id = $para['id'];
				$img = 'single';
				$sql="update {$pre}agile_images_sessions set dt='{$dt}', url='{$url}', path='{$path}' where id={$id}and sessionid='{$s_id}'";
				$wpdb->query($sql);
				
			}
			if($sqlname=='insert_user_image_aginst_sessionid'){
				$dt = date("Y-m-d H:i:s");
				$movefile = $para['movefile'];
				$url = $movefile['url'];
				$path = $movefile['file'];
				$s_id = $para['sessionid'];
				$qquuid = $movefile['qquuid'];
				//alog('$para',$para,__FILE__,__LINE__);
				$img = 'single';
				$sql = "insert into {$pre}agile_images_sessions (sessionid,dt,url,path,img,qquuid) values('{$s_id}','{$dt}','{$url}','{$path}','{$img}','{$qquuid}')";
				$wpdb->query($sql);
			}
			if($sqlname=='select_session_images'){
				$s_id = $para['sessionid'];
				$sql="Select * FROM {$wpdb->prefix}agile_images_sessions where sessionid='{$s_id}' and img='set'";
				$rs = $wpdb->get_results($sql);	
				return $rs;
			}
			if($sqlname=='select_session_user_images'){
				$s_id = $para['sessionid'];
				$sql="Select * FROM {$wpdb->prefix}agile_images_sessions where sessionid='{$s_id}' and img='single'";
				$rs = $wpdb->get_results($sql);	
				return $rs;
			}
			if($sqlname=='chk_sessionid_already_exist_user_img'){
				$s_id = $para['sessionid'];
				$sql="Select id FROM {$wpdb->prefix}agile_images_sessions where sessionid='{$s_id}' and img='single'";
				$rs = $wpdb->get_var($sql);	
				return $rs;
			}
		}
		
		function install(){
			global $wpdb;
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			$sql="
				CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."agile_images_sessions` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `sessionid` varchar(255) default NULL,
				  `dt` datetime default NULL,
				  `url` varchar(255) default NULL,
				  `path` varchar(255) default NULL,	
				  `img` varchar(255) default NULL,	
				  `qquuid` varchar(255) default NULL,	
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
				";
			dbDelta($sql);
		}
	} //class ends
} //class exists ends
new Agile_Front_End_Post();

?>
